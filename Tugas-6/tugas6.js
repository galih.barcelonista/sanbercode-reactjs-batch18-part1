//soal1
const luasLingkaran = (x) => {
    let hasil=3.14*x;
    return hasil;
}

const kelLingkaran = (x) => {
    let hasil=2*3.14*x;

    return hasil;
}

console.log(luasLingkaran(100));
console.log(luasLingkaran(200));

//soal2
let kal1 ="saya"
let kal2 ="adalah"
let kal3 ="seorang"
let kal4 ="frontend"
let kal5 ="developer"
let kalimat = `${kal1} ${kal2} ${kal2} ${kal3} ${kal4} ${kal5}`

console.log(kalimat)

//soal3
const newFunction = (firstName,lastName) =>{

    return {
        firstName,lastName,fullName: function(){
            console.log(`${firstName} ${lastName}`)
            return
        }
    }

}

console.log(newFunction("William","Imoh").fullName())

//soal4
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

  const {firstName,lastName,destination,occupation,spell} = newObject
  console.log(firstName,lastName,destination,occupation,spell)

  //soal5
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]

console.log(combined)
