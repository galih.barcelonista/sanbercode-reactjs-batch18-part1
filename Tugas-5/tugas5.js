// soal1
function halo(){
    return 'Halo Sanbers!'
}

console.log(halo());

//soal2
function kalikan(x,y){
    return x*y;
}

var num1=12;
var num2=4;

var hasilKali= kalikan(num1,num2)
console.log(hasilKali)

//soal3
function introduce(a,b,c,d){
    return "Nama saya "+a+' ,umur saya '+b+' tahun,alamat saya di '+c+', dan saya punya hobby yaitu '+d+'!'
}

var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"


var perkenalan = introduce(name,age,address.toLocaleLowerCase(),hobby);
console.log(perkenalan)

//soal4
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]

var biodata = {
    name: "Asep",
    gender: "laki-laki",
    hoby: "baca buku",
    year: 1992
}

console.log(arrayDaftarPeserta[0])
console.log(biodata.name)

//soal5

var fruit = [{nama: "strawberry",warna:"merah",ada_bijinya:"tidak",harga:9000},
             {nama: "jeruk",warna:"oranye",ada_bijinya:"ada",harga:8000},
             {nama: "Semangka",warna:"Hijau & Merah",ada_bijinya:"ada",harga:10000},
             {nama: "Pisang",warna:"kuning",ada_bijinya:"tidak",harga:5000},
            ]
var filterFruit = fruit.filter(function(item){
    return item.warna == "merah";
})

console.log(filterFruit)

//soal6
var dataFilm=[];

function cFilm(nama,durasi,genre,tahun){
    dataFilm.push({nama:nama,
                durasi:durasi,
                genre:genre,
                tahun:tahun});
    return dataFilm
}

cFilm('Stranger Things','60 menit','Thriller','2019');
cFilm('Infinity War','120 menit','Action','2018');
console.log(dataFilm)