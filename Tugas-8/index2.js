var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

function execute(time,book){
    readBooksPromise(time,book).then(
        function(item){
            if(item !==0){
                execute(item)
            }
        }
    )
}
 
execute(books)