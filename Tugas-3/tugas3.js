// soal 1
var kataPertama = "saya";
var kataKedua = "Senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var gabung = kataPertama+' '+kataKedua+' '+kataKetiga+' '+kataKeempat.toUpperCase()
console.log(gabung);

// soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var jumlah = parseInt(kataPertama)+parseInt(kataKedua)+parseInt(kataKetiga)+parseInt(kataKeempat)

console.log(jumlah);

// soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4,14);
var kataKetiga = kalimat.substring(15,18);
var kataKeempat = kalimat.substring(19,24);
var kataKelima = kalimat.substr(24)

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

// soal 4
var nilai = 90;

if(nilai>=80) {
    console.log("indeksnya A")
}else if(nilai>=70 && nilai<80) {
    console.log("indeksnya B")
}else if(nilai>=60 && nilai<70){
   console.log("indeksnya C")
}else if(nilai>=50 && nilai<60){
    console.log("indeksnya D")
}else{
    console.log("indeksnya E")
}

// soal 5
var tanggal = 3;
var bulan = 10;
var tahun = 1995;


switch(bulan) {
    case 1 :
        bulan='Januari'
        break;
    case 2 :
         bulan='Februari'
         break;
    case 3 :
         bulan='Maret'
        break;
    case 4 :
        bulan='April'
            break;
    case 5 :
        bulan='Mei'
             break;
    case 10 :
        bulan='Oktober'
            break;
    default :
        console.log('Not Valid!');
}

var tgLahir = tanggal+' '+bulan+' '+tahun

console.log(tgLahir)