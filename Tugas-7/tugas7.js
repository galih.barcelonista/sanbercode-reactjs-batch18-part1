//soal1
//release 0
class Animal{
        constructor(name) {
            this._name=name;
            this.legs=4;
            this.cold_blooded=false
        }

        get cnam(){
            return this._name;
        }

}

var sheep = new Animal("Shaun");

console.log(sheep.cnam);
console.log(sheep.legs);
console.log(sheep.cold_blooded);

//release 1

class Frog extends Animal {
    constructor(name){
        super(name);
    }

    jump(){
        console.log('Hop hop')
    }

}

class Ape extends Animal {
    constructor(name){
        super(name);
    }

        yell(){
        console.log('Auooooo')
    }

}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

// soal2

class Clock{
    constructor({template}){
        this._template = template
    }

    render(){
        var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
    }

    stop(){
        clearInterval(timer);
    }

    start(){
        render();
        timer = setInterval(render,1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 